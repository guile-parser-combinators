-*- org -*-

Guile Parser Combinators

* About

  A simple, SRFI-41 stream-based, monadic parser combinator library
  for Guile Scheme.

* Example

  #+BEGIN_SRC scheme
    (define stream->string (compose list->string stream->list))

    (define (parse-string str)
      (lambda (stream)
        (let ((input (stream->string (stream-take (string-length str) stream))))
          (if (string=? str input)
              (parse-result str (stream-drop (string-length str) stream))
              %parse-failure))))

    (define article
      (parse-any (parse-string "the ") (parse-string "a ")))

    (define noun
      (parse-any (parse-string "student ") (parse-string "professor ")))

    (define verb
      (parse-any (parse-string "studies ") (parse-string "lectures ")))

    (define noun-phrase
      (parse-each article noun))

    (define verb-phrase
      (parse-each verb noun-phrase))

    (define sentence
      (parse-each noun-phrase verb-phrase))

    (parse noun-phrase "the professor ")
    (parse sentence "the professor lectures the student ")
  #+END_SRC

* License

  GNU LGPL3+
