;; Per-directory local variables for GNU Emacs 23 and later.

((scheme-mode
  .
  ((indent-tabs-mode . nil)
   (eval . (put 'parse-match 'scheme-indent-function 1))
   (eval . (put 'stream-match 'scheme-indent-function 1)))))
