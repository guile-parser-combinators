(define-module (tests)
  #:use-module (srfi srfi-64)
  #:use-module (parser-combinators))

(test-begin "parser-combinators")

(test-assert "parse-failure?" (parse-failure? %parse-failure))

(test-group "parse-success?"
  (test-assert (parse-success? (parse-result "" 0)))
  (test-assert (not (parse-success? %parse-failure))))

(test-equal "parse-fail" (parse-fail "foo" 0) %parse-failure)

(test-equal "parse-return"
            ((parse-return "foo") "bar" 0)
            (parse-result "foo" 0))

(test-equal "parse-lift"
            (let ((parser ((parse-lift string-reverse) "foo")))
              (parser "bar" 0))
            (parse-result "oof" 0))

(test-group "parse-bind"
  (let ((parse-reverse (parse-lift string-reverse)))
    (test-equal (let ((parser (parse-bind (parse-return "foo") parse-reverse)))
                  (parser "bar" 0))
                (parse-result "oof" 0))
    (test-equal ((parse-bind parse-fail parse-reverse) "bar" 0)
                %parse-failure)))

(test-group "parse-alt"
  (test-equal ((parse-alt) "foo" 0) %parse-failure)
  (test-equal ((parse-alt (parse-return "bar") parse-fail) "foo" 0)
              (parse-result "bar" 0))
  (test-equal ((parse-alt parse-fail (parse-return "bar")) "foo" 0)
              (parse-result "bar" 0)))

(test-group "parse-seq"
  (test-equal ((parse-seq) "foo" 0)
              (parse-result '() 0))
  (test-equal ((parse-seq (parse-return "bar")) "foo" 0)
              (parse-result '("bar") 0))
  (test-equal ((parse-seq (parse-return "bar") (parse-return "baz")) "foo" 0)
              (parse-result '("bar" "baz") 0))
  (test-equal ((parse-seq (parse-return "bar") parse-fail) "foo" 0)
              %parse-failure)
  (test-equal ((parse-seq parse-fail (parse-return "bar")) "foo" 0)
              %parse-failure))

(test-group "parse"
  (test-equal (parse (parse-return "foo") "bar") "foo")
  (test-equal (parse parse-fail "foo") #f)
  (test-equal (parse parse-fail "foo" 'oops) 'oops))

(test-end "parser-combinators")
